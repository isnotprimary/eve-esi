# Eve ESI
Auto generated, ES6 promise based swagger client for Eve online ESI Api.

## Local build
To run the build locally you need a working docker install and then run the following:
```bash
# docker run --rm -e JAVA_OPTS="-Xmx2048M -DloggerPath=conf/log4j.properties" -v ${PWD}:/local openapitools/openapi-generator-cli generate     -i /local/swagger.json     -g javascript     -o /local/out/go     --additional-properties usePromises=true --additional-properties projectName=eveEsi,projectVersion0.0.7,useES6=true
```

To get the latest version of the api from Eve run:
```bash
# ./getLatestApi.sh
```

Currently, the library version is hardcoded in `.gitlab-ci.yml` to override the one in the API

